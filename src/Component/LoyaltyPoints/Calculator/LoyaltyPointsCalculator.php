<?php

namespace App\Component\LoyaltyPoints\Calculator;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoyaltyPointsCalculator extends AbstractController implements LoyaltyPointsCalculatorInterface
{
    public static $multiplier = [
        1 => 3,
        2 => 3,
        3 => 3,
        4 => 3,
        5 => 4,
        6 => 5,
        7 => 5
    ];

    public function calculateLoyaltyPoints(string $pricingPlanName, int $loyaltyPoints): int
    {
        return $loyaltyPoints * $this->getLoyaltyPointsMultiplier($pricingPlanName);
    }

    public function getLoyaltyPointsMultiplier(string $pricingPlanName): int
    {
        if ($pricingPlanName === 'default') {
            return 1;
        }

        if ($pricingPlanName === 'premium') {
            $weekday = date('w');
            return isset(self::$multiplier[$weekday]) ? self::$multiplier[$weekday] : 1;
        }

        throw new \InvalidArgumentException('Invalid pricing plan name');
    }
}