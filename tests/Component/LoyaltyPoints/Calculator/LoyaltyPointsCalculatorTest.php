<?php

namespace App\Tests\Component\LoyaltyPoints\Calculator;

use App\Component\LoyaltyPoints\Calculator\LoyaltyPointsCalculator;
use PHPUnit\Framework\TestCase;
use phpmock\phpunit\PHPMock;

class LoyaltyPointsCalculatorTest extends TestCase
{
    use PHPMock;

    private $calculator;

    protected function setUp(): void
    {
        $this->calculator = new LoyaltyPointsCalculator();
    }

    /**
     * @dataProvider weekdayDataProvider
     */
    public function testCalculateLoyaltyPoints($expectedMultiplier)
    {
        $datetimeMock = $this->getFunctionMock('App\Component\LoyaltyPoints\Calculator', 'date');
        $datetimeMock->expects($this->any())->willReturn($expectedMultiplier);

        $points = 100;
        $expected = $points * LoyaltyPointsCalculator::$multiplier[$expectedMultiplier];
        $points = $this->calculator->calculateLoyaltyPoints('premium', $points);

        $this->assertEquals($expected, $points);
    }

    public function weekdayDataProvider()
    {
        return [
            [1],//Monday
            [2],//Tuesday
            [3],//Wednesday
            [4],//Thursday
            [5],//Friday
            [6],//Saturday
            [7],//Sunday
        ];
    }
}
