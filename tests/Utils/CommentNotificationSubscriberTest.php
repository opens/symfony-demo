<?php

namespace App\Tests\Utils;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use App\Event\CommentCreatedEvent;
use App\EventSubscriber\CommentNotificationSubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommentNotificationSubscriberTest extends TestCase
{
    private $mailer;
    private $urlGenerator;
    private $translator;

    protected function setUp(): void
    {
        $this->mailer = $this->createMock(MailerInterface::class);
        $this->urlGenerator = $this->createMock(UrlGeneratorInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
    }

    public function testOnCommentCreated()
    {
        $post = new Post();
        $post->setSlug('test-post');

        $user = new User();
        $user->setEmail('test@example.com');
        $post->setAuthor($user);

        $comment = new Comment();
        $comment->setPost($post);

        $event = new CommentCreatedEvent($comment);

        $subscriber = new CommentNotificationSubscriber($this->mailer, $this->urlGenerator, $this->translator, 'no-reply@example.com');

        $this->mailer->expects($this->once())->method('send')->with($this->callback(function($subject) {
            $this->assertInstanceOf(Email::class, $subject);
            $this->assertSame('no-reply@example.com', $subject->getFrom()[0]->getAddress());
            $this->assertSame('test@example.com', $subject->getTo()[0]->getAddress());
            return true;
        }));

        $subscriber->onCommentCreated($event);
    }
}
