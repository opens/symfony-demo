# FA Recruitment process task

### Recommendations

- Use the standards provided by Symfony
- Take care about code style
- Take care of security
- Commit your work properly using git

### Prepare your environment

What you need:

- PHP 8.1
- MySQL 8.0
- Webserver

Fork GitHub repository `https://github.com/symfony/demo`  

Create a feature branch based on `main` which you will be working on

* If you have knowledge from Docker you can prepare an environment based on point `0`


0. **(Optional)** Add simple docker-compose configuration containing php `8.1` with composer 2 (max version `2.4` lock
   with current sha256 digest),
   nginx-mainline, mysql 8.0 with default database name `symfony`.

## Task

1. Customer wants to move the project to one step forward by changing sqlite database to `mysql 8.0`.
    - Provide proper changes in the project configuration. (Keep other databases connection string commented
      Hint: `.env:29`),
    - Take care of database schema versioning.

2. Registration option which will the pass following requirements
    - Add a form containing the following fields
        - Username,
        - Full name,
        - Email,
        - Passwords (length: `10` - `20` characters).
    - Add registration controller route should be `/register` and route should be named `register`,
    - Add registration button `Register` (in Login view) is in the same level as `Sign in` button and has a link to
      register route,
    - Add translations to the `Register` button to `English` and `Polish` language.

   #### Requirements / Acceptance criteria:
    - Client should be able to register,
    - Client cannot register with the same Username and/or Password,
    - Validation should work properly,
    - Registration route should be `/register` and route name `register`.

3. Update following recipes
   - `doctrine/doctrine-bundle`,
   - `symfony/apache-pack`,
   - `symfony/mailer`,
   - `symfony/phpunit-bridge`,
   - `symfony/webpack-encore-bundle`

4. Add unit test for `App\EventSubscriber\CommentNotificationSubscriber`

5. Add pricing plan to User
   - Add pricing plan to the User entity
     - `VARCHAR(10)`,
     - Max length `10` characters,
     - Field is not nullable in the database,
     - Default price plan has name `default`,
     - Take care of database schema versioning and current data in it.

   - Add implementation of 2 pricing plans options
     1. `default` has always loyalty points multiplier as `1`,
     2. `premium` depends on the weekday has different loyalty points multiplier presented in the following table:

     | Week day | loyalty points multiplier |
     |----------|---------------------------|
     | Mo-Th    | 3                         |
     | Fr       | 4                         |
     | Sa-Su    | 5                         |
      
      - Add unit test to created `premium` pricing plan.

   - Attach interface to the project and add an implementation of the following interface:
      ```php
      ...

      namespace App\Component\LoyaltyPoints\Calculator;

      interface LoyaltyPointsCalculatorInterface
      {
          public function calculateLoyaltyPoints(string $pricingPlanName, int $loyaltyPoints): int;
      
          public function getLoyaltyPointsMultiplier(string $pricingPlanName): int;
      }
      ```

      - `calculateLoyaltyPoints` should multiply `$loyaltyPoints` by the value returned by `getLoyaltyPointsMultiplier`
         method
      - `getLoyaltyPointsMultiplier` should return the correct multiplier based on `$pricingPlanName` parameter
        and the `default` and `premium` pricing plans implementations.

6. Dump your project database using `mysqldump` and attach it in commit to the repository in `dump.sql`